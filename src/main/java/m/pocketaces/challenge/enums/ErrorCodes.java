package m.pocketaces.challenge.enums;

public enum ErrorCodes {
	
	
	PARENT_NOT_FOUND("TSE-1001","Parent doesnot exists."),
	TXN_NOT_FOUND("TSE-1002","Txn doesnot exist's in database");
	
	
	String code;
	String message;
	
	private ErrorCodes(String code,String message) {
		this.code = code;
		this.message = message;
	}
	
	
	public String getCode() {
		return this.code;
	}
	
	public String getMessage() {
		return this.message;
	}

}
