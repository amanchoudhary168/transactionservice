package m.pocketaces.challenge.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import m.pocketaces.challenge.entity.TransactionDetails;
import m.pocketaces.challenge.enums.ErrorCodes;
import m.pocketaces.challenge.exceptions.TransactionServiceException;
import m.pocketaces.challenge.models.TransactionCreateVo;
import m.pocketaces.challenge.models.TransactionDetailsVo;
import m.pocketaces.challenge.repository.TransactionRepository;

@Service
public class TransactionService {
	
	@Autowired
	TransactionRepository transactionRepository;
	
	private static final Logger LOG = LoggerFactory.getLogger(TransactionService.class);
	
	
	public boolean saveCreateRequest(TransactionCreateVo transactionCreateVo) {
		
		TransactionDetails currentTransaction = new TransactionDetails();
		
		currentTransaction.setParentId(transactionCreateVo.getParentId());
		currentTransaction.setTransactionId(transactionCreateVo.getTransactionId());
		currentTransaction.setType(transactionCreateVo.getType());
		currentTransaction.setAmount(transactionCreateVo.getAmount());
		
		transactionRepository.save(currentTransaction);
		 
		if(transactionCreateVo.getParentId() != -1) {
			LOG.debug("Transaction has Parent ,Parent-Id {} ,Checking parent Details.",transactionCreateVo.getParentId());
			try {
				TransactionDetails currentParent = transactionRepository.getById(transactionCreateVo.getParentId());
				LOG.debug("Fetched Parent Transaction details = {}",currentParent);
				Set<TransactionDetails> childTransactions = currentParent.getChildTransactions();
				if(childTransactions == null)
					childTransactions = new HashSet<>();
				childTransactions.add(currentTransaction);
				transactionRepository.save(currentParent);
				LOG.debug("Parent transation updated Successfully.");
				
			}catch (EntityNotFoundException ex) {
				throw new TransactionServiceException(ErrorCodes.PARENT_NOT_FOUND.getCode(), ErrorCodes.PARENT_NOT_FOUND.getMessage());
			}
		}
		
		LOG.debug("Current transation saved Successfully.Id {}",currentTransaction.getTransactionId());
			
		return true;
	}
	
	public TransactionDetailsVo findTransactionDetails(long id){
		Optional<TransactionDetails> transactionDetails = transactionRepository.findById(id);
		if (!transactionDetails.isPresent()) {
			LOG.info("Transaction does not exist in the database. Id ={}",id);
			throw new TransactionServiceException(ErrorCodes.TXN_NOT_FOUND.getCode(), ErrorCodes.TXN_NOT_FOUND.getMessage());
		}
		TransactionDetails txnEntity =  transactionDetails.get();
		TransactionDetailsVo txnVo = new TransactionDetailsVo();
		txnVo.setAmount(txnEntity.getAmount());
		txnVo.setParentId(txnEntity.getParentId());
		txnVo.setTransactionId(txnEntity.getTransactionId());
		txnVo.setType(txnEntity.getType());
		
		return txnVo;
		
	}
	
	public List<Long> findTransactionList(String type){
		List<TransactionDetails> transactions = transactionRepository.findByType(type);
		List<Long> transactionIdList = new ArrayList<>();
		for(TransactionDetails txn : transactions)
			transactionIdList.add(txn.getTransactionId());	
		return transactionIdList;
	}
	
	
	public Double calculateSum(Long transactionId) {
		Optional<TransactionDetails> transactionDetails = transactionRepository.findById(transactionId);
		if (!transactionDetails.isPresent()) {
			LOG.info("Transaction does not exist in the database. Id ={}",transactionId);
			throw new TransactionServiceException(ErrorCodes.TXN_NOT_FOUND.getCode(), ErrorCodes.TXN_NOT_FOUND.getMessage());
		}
		TransactionDetails txnEntity =  transactionDetails.get();
		Double totalSum = calculateTotalTxnAmount(txnEntity);
		return totalSum;
		
		
	}
	
	private Double calculateTotalTxnAmount(TransactionDetails txnDetails) {
		Double totalAmount = txnDetails.getAmount();
		if(txnDetails.getChildTransactions() != null && !txnDetails.getChildTransactions().isEmpty()) {
			for(TransactionDetails tDetails : txnDetails.getChildTransactions()) {
				totalAmount+= calculateTotalTxnAmount(tDetails);
			}	
		}
		return totalAmount;
	}
	
	

}
