package m.pocketaces.challenge.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class TransactionDetails {
	
	@Id
	long transactionId;
	
	long parentId;
	
	String type;
	
	double amount;
	
	@OneToMany
	Set<TransactionDetails> childTransactions;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<TransactionDetails> getChildTransactions() {
		return childTransactions;
	}

	public void setChildTransactions(Set<TransactionDetails> childTransactions) {
		this.childTransactions = childTransactions;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "TransactionDetails [transactionId=" + transactionId + ", parentId=" + parentId + ", type=" + type
				+ ", amount=" + amount + ", childTransactions=" + childTransactions + "]";
	}
	
	
	
	
	
	
}
