package m.pocketaces.challenge.exceptions;

public class TransactionServiceException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String errorCode;
	String errorMessage;
	
	public TransactionServiceException(String errorCode,String errorMessage) {
		super(errorCode+":"+errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	@Override
	public String toString() {
		return "ParentException [errorCode=" + errorCode + ", errorMessage=" + errorMessage + "]";
	}
	
	
	

}
