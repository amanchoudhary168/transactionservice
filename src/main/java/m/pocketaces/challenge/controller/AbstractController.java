package m.pocketaces.challenge.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import m.pocketaces.challenge.enums.STATUS;
import m.pocketaces.challenge.exceptions.TransactionServiceException;
import m.pocketaces.challenge.model.reponse.GenericResponse;

public class AbstractController {
	
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(TransactionServiceException.class)
	public GenericResponse handleParentException(HttpServletRequest req,Exception ex) {
		TransactionServiceException exception = (TransactionServiceException) ex;
		
		GenericResponse response = new GenericResponse();
		response.setStatus(STATUS.FAILURE);
		response.setErrorCode(exception.getErrorCode());
		response.setErrorMessage(exception.getErrorMessage());
		return response;
	}

}
