package m.pocketaces.challenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import m.pocketaces.challenge.enums.STATUS;
import m.pocketaces.challenge.model.reponse.CreateResponse;
import m.pocketaces.challenge.model.reponse.SumResponse;
import m.pocketaces.challenge.model.reponse.TransactionDetailsResponse;
import m.pocketaces.challenge.model.reponse.TransactionIdListResponse;
import m.pocketaces.challenge.model.request.TransactionCreateRequest;
import m.pocketaces.challenge.models.TransactionCreateVo;
import m.pocketaces.challenge.models.TransactionDetailsVo;
import m.pocketaces.challenge.service.TransactionService;

@RestController
@RequestMapping("/transactionservice/transaction/*")
public class TransactionDetailsController extends AbstractController {
	
	@Autowired
	TransactionService transactionService;
	
	
	@SuppressWarnings("deprecation")
	@PutMapping("/{transactionId}")
	public CreateResponse saveTransaction(@PathVariable long transactionId ,
			@RequestBody TransactionCreateRequest createTransactionRequest) {
		
		TransactionCreateVo transactionCreateVo = new TransactionCreateVo();
		transactionCreateVo.setAmount(createTransactionRequest.getAmount());
		transactionCreateVo.setType(createTransactionRequest.getType());
		if(!StringUtils.isEmpty(createTransactionRequest.getParentId()))
			transactionCreateVo.setParentId(Long.parseLong(createTransactionRequest.getParentId()));
		else
			transactionCreateVo.setParentId(-1);
		
		transactionCreateVo.setTransactionId(transactionId );
		
		boolean response = transactionService.saveCreateRequest(transactionCreateVo);
		
		CreateResponse createResponse = new CreateResponse();
		createResponse.setStatus(STATUS.FAILURE);
		if(response)
			createResponse.setStatus(STATUS.SUCCESS);
		
		return createResponse;
	
	}
	
	@GetMapping("/details/{id}")
	public TransactionDetailsResponse getTransactionDetails(@PathVariable Long id) {
		
		TransactionDetailsVo transactionVo = transactionService.findTransactionDetails(id);
		TransactionDetailsResponse response = new TransactionDetailsResponse();
		response.setStatus(STATUS.SUCCESS);
		response.setTxnDetails(transactionVo);
		return response;
	}
	
	
	@GetMapping("/sum/{id}")
	public SumResponse getTotalSum(@PathVariable Long id) {
		Double totalSum = transactionService.calculateSum(id);
		SumResponse response = new SumResponse();
		response.setStatus(STATUS.SUCCESS);
		response.setTotalSum(totalSum);
		return response;
	}
	
	@GetMapping("/type/{type}")
	public TransactionIdListResponse getTransactionByType(@PathVariable String type) {
		List<Long> transactionList = transactionService.findTransactionList(type);
		TransactionIdListResponse response = new TransactionIdListResponse();
		response.setStatus(STATUS.SUCCESS);
		response.setTransactionList(transactionList);
		return response;
	}
}
