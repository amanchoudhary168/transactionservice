package m.pocketaces.challenge.model.request;

public class TransactionCreateRequest {
	
	String parentId;
	String type;
	Double Amount;
	
	
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getAmount() {
		return Amount;
	}
	public void setAmount(Double amount) {
		Amount = amount;
	}
	
	
	
	
	
	

}
