package m.pocketaces.challenge.model.reponse;

import java.util.List;

public class TransactionIdListResponse extends GenericResponse{
	
	List<Long> transactionList;

	public List<Long> getTransactionList() {
		return transactionList;
	}

	public void setTransactionList(List<Long> transactionList) {
		this.transactionList = transactionList;
	}
	

}
