package m.pocketaces.challenge.model.reponse;


import m.pocketaces.challenge.models.TransactionDetailsVo;

public class TransactionDetailsResponse extends GenericResponse {
	
	TransactionDetailsVo txnDetails;

	public TransactionDetailsVo getTxnDetails() {
		return txnDetails;
	}

	public void setTxnDetails(TransactionDetailsVo txnDetails) {
		this.txnDetails = txnDetails;
	}
	
	
	
}
