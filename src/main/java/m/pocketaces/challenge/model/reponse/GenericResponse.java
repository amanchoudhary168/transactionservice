package m.pocketaces.challenge.model.reponse;

import com.fasterxml.jackson.annotation.JsonInclude;

import m.pocketaces.challenge.enums.STATUS;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResponse {
	
	STATUS status;
	String errorCode;
	String errorMessage;
	
	
	public STATUS getStatus() {
		return status;
	}
	public void setStatus(STATUS status) {
		this.status = status;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
	
	

}
