package m.pocketaces.challenge.model.reponse;

public class SumResponse extends GenericResponse{
	Double totalSum;

	public Double getTotalSum() {
		return totalSum;
	}

	public void setTotalSum(Double totalSum) {
		this.totalSum = totalSum;
	}
	
	

}
