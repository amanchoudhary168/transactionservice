package m.pocketaces.challenge.model.reponse;

import m.pocketaces.challenge.enums.STATUS;

public class CreateResponse {
	
	STATUS status;

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}
	

}
