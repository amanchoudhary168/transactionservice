package m.pocketaces.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocketAceChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocketAceChallengeApplication.class, args);
	}

}
