<h3>Description</h3>
Transaction service is a project which provides api's to save,find and delete transaction from the database.<br>
It also provides an api which will find sum of the transaction amount which are transitively linked.<br>

<h3>Technology Used</h3>
    <ul>
        <li>Spring Boot</li>
        <li>Spring Data</li>
        <li>Java 8</li>
        <li>H2 Database</li>
    </ul>


<h3>Setting up database</h3>
<b>Step 1:</b>
    <br>Download the file pocketaces.mv.db from repository and place it in a directory.
<br><b>Step 2:</b>
    <br>Add the location of the file in the application.properties.

<h3>How to run the project</h3>
<b>Step 1:</b>
    Take the checkout of the project and run mvn install command,it will create an executable jar in the target folder.
<br><b>Step 2:</b>
    <br>Run command java -jar /<path-of-the-jar>/<jarname>.jar

<h3>License</h3[]([url](url))>
MIT
